package Levely.Level_2;

import main.*;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import PribehyLevely.Pribeh1;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Level2 {
	ImageIcon drozdi = Okno.resizeImage(new ImageIcon("rsc/drozdi.png"), 200, 200);
	Okno okno;
	Vozik vozik;
	public static Panel hraPanel;
	static Thread t;
	public Level2(Okno okno) {
		this.okno = okno;
		okno.setTitle("Šňupejte droždí - Level 2 - NÁKUP");
		this.zaklad(okno);
		okno.repaint();
		//cekani na ukonceni
		synchronized (this) {
			try {
				wait();
			} catch (InterruptedException ex) {
				System.out.println("CHYBA -- Level2");
			}
		}
		System.out.println("Level2 -- KONEC    " + Thread.currentThread());
	}

	void zaklad(Okno okno) {
		okno.smazat();
		okno.defOkno();
		
		okno.otazkyLabel.setLayout(null);
		okno.otazkyLabel.setFont(new Font("Consolas", Font.PLAIN, 35));
		okno.otazkyLabel.setForeground(Color.white);
		okno.otazkyLabel.setBounds(0, okno.vyskaOkna * 5 / 8, okno.sirkaOkna, okno.vyskaOkna * 1 / 8);
		okno.otazkyLabel.setBackground(Color.red);
		okno.otazkyLabel.setOpaque(false);
		okno.otazkyLabel.setVerticalTextPosition(JLabel.BOTTOM);
		
		okno.otazkyPanel.setOpaque(true);
		okno.otazkyPanel.setBackground(Color.black);

		okno.odpovediPanel.setLayout(null);
		okno.odpovediPanel.setOpaque(true);
		hraPanel = new Panel(okno);
		
		okno.addKeyListener(hraPanel.keyK);
		okno.setFocusable(true);
		okno.requestFocus();
		
		okno.add(hraPanel);
		hraPanel.setVisible(true);
	}
}
