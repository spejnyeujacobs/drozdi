package Levely.Level_0;

import main.RelatvniVelikost;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import main.Main;
import main.Okno;

public class Level0 {
	Okno okno;
	Thread t;
	FileManager fileManager;
	public Level0(Okno okno) {
		this.okno = okno;
		fileManager = new FileManager();
		t = Thread.currentThread();
		zaklad(okno);
		// okno.setVisible(true);
		okno.repaint();
		// cekani dokud nezmackne konecna tlacitko
		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException ex) {
				System.out.println("CHYBA -- Level0");
			}

		}
		okno.smazat();
		System.out.println("KONEC Level0");
	}

	void zaklad(Okno okno) {
		okno.smazat();
		okno.defOkno();
		
		okno.hlPanel.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 100));
		okno.hlPanel.setLayout(null);
		okno.hlPanel.setOpaque(false);
		okno.add(okno.hlPanel);
		// pozadi
		JLabel pozadi = new JLabel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.drawImage(FileManager.nesnupejteDrozdi, 0, 0, this.getSize().width, this.getSize().height, null);
			}
		};
		pozadi.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 100));
		pozadi.setOpaque(false);
		okno.hlPanel.add(pozadi);
		
		//levely tlacitka
		pozadi.add(tlacitkoLevel(pozadi, RelatvniVelikost.obdelnik(10, 35, 20, 10), 1));
		pozadi.add(tlacitkoLevel(pozadi, RelatvniVelikost.obdelnik(10, 50, 20, 10), 2));
		pozadi.add(tlacitkoLevel(pozadi,  RelatvniVelikost.obdelnik(10, 65, 20, 10), 3));
		//level3 mapy
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(15, 80, 7, 10), 0));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(25, 80, 7, 10), 1));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(35, 80, 7, 10), 2));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(45, 80, 7, 10), 3));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(55, 80, 7, 10), 4));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(65, 80, 7, 10), 5));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(75, 80, 7, 10), 6));
		//cas a statictiky
		JLabel statistika = new JLabel() {
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(java.awt.Color.blue);
				g2d.fillRect(0,0,this.getSize().width, this.getSize().height);
				//g2d.drawImage(FileManager.nesnupejteDrozdi, 0, 0, this.getSize().width, this.getSize().height, null);
				g2d.setColor(java.awt.Color.yellow);
				g.setFont(g.getFont().deriveFont(12f));
				g.drawString("Časy:", 20, 20);
				
				g2d.drawString("Level1:           "+ prevodCasu(Main.casLevel1), 20, 40);
				    g2d.drawString("Level2:           "+ prevodCasu(Main.casLevel2), 20, 60);
				for (int i=0;i<Main.mapaCasy.length;i++) {
					g2d.drawString("Level3/mapa"+ i +":       "+ prevodCasu(Main.mapaCasy[i]), 20, 80+20*i);	
					
				}
			}};
		statistika.setBounds(RelatvniVelikost.obdelnik(35,35, 20, 40));
		
		pozadi.add(statistika);
		//discord talcitko
		JButton discordTlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/discord.png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni obrazku dircord" + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		discordTlacitko.addActionListener(e -> {
		 openWebpage("https://discord.gg/AzBmaPWQGR");
		});
		okno.hlPanel.setOpaque(false);
		discordTlacitko.setBounds(RelatvniVelikost.obdelnik(90, 80, 7, 10));
		discordTlacitko.setVisible(true);
		pozadi.add(discordTlacitko);
	}

	private JButton tlacitkoLevel3Mapa(JLabel pozadi, Rectangle rect, int mapa) {
		// tlacitko Levely
		JButton tlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/mapa" + mapa + ".png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni mapy " + mapa + " " + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		tlacitko.addActionListener(e -> {
			Main.postup = 3;
			Main.level3Level = mapa;
			konec();
		});
		okno.hlPanel.setOpaque(false);
		tlacitko.setBounds(rect);
		tlacitko.setVisible(true);
		// tlacitko.repaint();
		return tlacitko;
	}

	public void konec() {
		synchronized (this) {
			notify();
		}

	}

	private JButton tlacitkoLevel(JLabel label, Rectangle rect, int cisloLevelu) {
		// tlacitko Levely
		JButton tlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/level" + cisloLevelu + ".png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni levelu " + cisloLevelu + " " + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		tlacitko.addActionListener(e -> {
			Main.postup = cisloLevelu;
			konec();
		});
		okno.hlPanel.setOpaque(false);
		tlacitko.setBounds(rect);
		tlacitko.setVisible(true);
		// tlacitko.repaint();
		return tlacitko;
	}
	
	private String prevodCasu(long cas) {
		if (cas == 2147483647) {
			return "??????";
		}
		//String vysledek = null;
		String sekundyText = Long.toString((cas % 60000)/1000);
		if ((cas % 60000)/1000 *60 < 10) {
			sekundyText = "0" + sekundyText;
		}
		String milisekundyText = Long.toString((cas % 60000)%1000);
		if ((cas % 60000)%1000 < 10) {
			milisekundyText = "0" + milisekundyText;
		}
		return (cas / 60000 + ":" + sekundyText + ":" + milisekundyText);
	}
	//zkopcena metoda na otvirani 
	public static void openWebpage(String urlString) {
	    try {
	        java.awt.Desktop.getDesktop().browse(new URL(urlString).toURI());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

}
