package Levely.Level_3;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import java.awt.Point;

import main.Okno;

public class FileManager {
	public static BufferedImage hracR, hracL, hracU, hracD;
	public static BufferedImage strela;
	public static BufferedImage  paleta;
	public static BufferedImage stena, bodak, zebrik,vez, checkpoint, klic, dvere, dvereOpen;
	public static Point[] pocatecniPoziceMap;

	public FileManager() {
		pocatecniPoziceMap= new Point[]{new Point(13, 10),new Point(13, 10),new Point(13, 10)};
		// mapa
		try {
			paleta = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/mapy/nastaveni_barvy.bmp")));
		} catch (Exception e) {
			System.out.println("CHYBA -- Level3 -- Nacteni obrazku palety" + e);
		}
		// nacteni obrazku hrace
		try {
			if (hracR == null) {
				hracR = ImageIO.read(new File(Okno.ziskatCestu("rsc/player/right.png")));
			}
			if (hracL == null) {
				hracL = ImageIO.read(new File(Okno.ziskatCestu("rsc/player/left.png")));
			}
			if (hracU == null) {
				hracU = ImageIO.read(new File(Okno.ziskatCestu("rsc/player/up.png")));
			}
			if (hracD == null) {
				hracD = ImageIO.read(new File(Okno.ziskatCestu("rsc/player/down.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani obrazku hrace");
		}
		try {
			if (bodak == null) {
				bodak = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/bodak.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani obrazku bodaku");
		}
		try {
			if (stena == null) {
				stena = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/stena.png"))); // bodak2.png
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani obrazku steny");
		}
		try {
			if (strela == null) {
				strela = ImageIO.read(new File(Okno.ziskatCestu("rsc/drozdi.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani strely");
		}
		try {
			if (vez == null) {
				vez = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/vez.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani veze");
		}
		try {
			if (zebrik == null) {
				zebrik = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/zebrik.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani zebriku");
		}
		try {
			if (checkpoint == null) {
				checkpoint = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/checkpoint.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani checkpointu");
		}
		try {
			if (klic == null) {
				klic = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/klic.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani klice");
		}
		try {
			if (dvere == null) {
				dvere = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/door.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani dvere");
		}
		try {
			if (dvereOpen == null) {
				dvereOpen = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/doorOpen.png")));
			}
		} catch (Exception e) {
			System.out.println("CHYBA -- nacitani dvereOpen");
		}
	}

}
