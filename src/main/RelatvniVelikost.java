package main;

import java.awt.Point;
import java.security.spec.ECPoint;
import java.awt.Rectangle;

public class RelatvniVelikost {
	private static Point velikostOkna = new Point();

	public static void setMaximum(int x, int y) {
		velikostOkna.x = x;
		velikostOkna.y = y;
	}
	public static Point getMaximum() {
		return velikostOkna;
	}

	synchronized public static int procentoX(int procento) {
		return procento * velikostOkna.x / 100;
	}

	synchronized public static int procentoY(int procento) {
		return procento * velikostOkna.y / 100;
	}

	synchronized static public Point procentoPoint(int procentox, int procentoy) {
		return new Point(procentoX(procentox), procentoY(procentoy));
	}

	synchronized static public Rectangle obdelnik(int procentox, int procentoy, int velikostx, int velikosty) {
		return new Rectangle(procentoX(procentox), procentoY(procentoy), procentoX(velikostx), procentoY(velikosty));
	}
	
	//zadane maxima
	synchronized public static int procentoX(int procento,int max) {
		return procento * max / 100;
	}

	synchronized public static int procentoY(int procento, int max) {
		return procento * max/ 100;
	}

	synchronized static public Point procentoPoint(int procentox, int procentoy,int max) {
		return new Point(procentoX(procentox, max), procentoY(procentoy, max));
	}
	/**
	 * @param procentox  kolik procent z maxX ma byt horni levy roh
	 * @param procentoy  kolik procent z maxY ma byt horni levy roh
	 * @param velikostx  kolik procent z maxX ma byt sirka
	 * @param velikosty  kolik procent z maxY ma byt vyska
	 */
	synchronized static public Rectangle obdelnik(int procentox, int procentoy, int velikostx, int velikosty,int maxX, int maxY) {
		return new Rectangle(procentoX(procentox, maxX), procentoY(procentoy, maxY), procentoX(velikostx, maxX), procentoY(velikosty, maxY));
	}
	
}
