package Levely.Level_3;

import main.*;

import javax.swing.JLabel;
import Levely.Level_3.steny.Klic;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.util.ArrayList;

public class Level3 {
	Okno mistniOkno;
	public static Panel_level3 hraPanel;
	static Thread t;
	public boolean konec = true;
	public Point PocatecniPozice, PoziceOprotiObrazovce, zakladniPoziceOprotiObrazovce;
	public Point posun;
	JLabel odpovediLabel;
	public ArrayList<Klic> ulozeneKlice;
	//label info
	
	public int pocetZdechnuti = 0;
	public int pocetKlicu = 0;
	int mapa = 0;
	long cas;
	public Level3(Okno okno) {
		mapa = Main.level3Level;
		mistniOkno = okno;
		zaklad(okno);
		okno.setTitle("Šňupejte droždí  - Level 3 - CESTA DOMŮ");
		new FileManager();
		//PocatecniPozice = new Point(13, 10); //v kostickach
		zakladniPoziceOprotiObrazovce = new Point(13, 10);
		PoziceOprotiObrazovce = new Point(13, 10);
		PocatecniPozice = (Point) PoziceOprotiObrazovce.clone();
		posun = new Point(0, 0);
		okno.repaint();
		cas = System.currentTimeMillis();
		do {
			this.zakladPanel(okno);
			// cekani na ukonceni
			t = Thread.currentThread();
			
			synchronized (t) {
				try {
					t.wait();
				} catch (InterruptedException ex) {
					System.out.println("CHYBA -- Level3");
				}
			}
			pocetZdechnuti++;
		} while (konec == false);
		Main.level3Level = mapa;
		System.out.println("Level3 -- KONEC    " + Thread.currentThread());
	}
	public void ulozeniCasu() {
		cas = System.currentTimeMillis() - cas;
		if (Main.mapaCasy[mapa] > cas) {
			Main.mapaCasy[mapa]= cas;
		}
		mapa++;
	}
	private void zaklad(Okno okno) {
		okno.smazat();
		odpovediLabel = new JLabel();
		odpovediLabel.setLayout(null);
		
		
		odpovediLabel.setText("Úmrtí v důsledku závislosti: "+ pocetZdechnuti + "Klíče: " + pocetKlicu+ "/5");
		odpovediLabel.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 15));
		odpovediLabel.setHorizontalAlignment(JLabel.CENTER);
		//odpovediLabel.setHorizontaltPosition(JLabel.CENTER);
		odpovediLabel.setVerticalTextPosition(JLabel.CENTER);
		odpovediLabel.setFont(new Font("Consolas", Font.PLAIN, 35));
		odpovediLabel.setForeground(Color.white);
		
		okno.odpovediPanel.setLayout(null);
		okno.odpovediPanel.setOpaque(true);
		okno.odpovediPanel.setBackground(new Color(77,68,68));
		okno.odpovediPanel.setBounds(RelatvniVelikost.obdelnik(0, 85, 100, 15));
		okno.odpovediPanel.add(odpovediLabel);
		
		okno.setVisible(true);
		okno.setFocusable(true);
		okno.add(okno.odpovediPanel);
		
	}

	void zakladPanel(Okno okno) {
		
		hraPanel = new Panel_level3(okno, this);
		okno.setFocusable(true);
		okno.requestFocus();

		okno.add(hraPanel);
		okno.addKeyListener(new PohybVstup(hraPanel));
	}
}