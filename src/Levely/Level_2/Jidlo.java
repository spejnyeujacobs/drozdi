package Levely.Level_2;


public class Jidlo {
	public String jmeno;
	public static double rychlost = 2;
	public int x;
	public int y;
	public static int velikost = 50;

	public Jidlo(String jmeno, int x, int y) {
		this.jmeno = jmeno;
		this.x = x;
		this.y = y;
	}

	public boolean kolize(Vozik vozik) {
		boolean kontrolaX = this.jeMezi( this.x, vozik.misto, vozik.misto + vozik.sirka)|| this.jeMezi(this.x + velikost, vozik.misto, vozik.misto + vozik.sirka);
		boolean kontrolaY = this.jeMezi(this.y, vozik.souradniceY, vozik.souradniceY + vozik.vyska/2)|| this.jeMezi(this.y + velikost, vozik.souradniceY, vozik.souradniceY + vozik.vyska/2);
		return (kontrolaX&&kontrolaY);
	}

	private boolean jeMezi(int x, int a, int b) { // zkontroluje zda je x mezi a a b, uzavreny interval
		if (x <= a) {
			return b <= x;
		}else {
			return b >= x;
		}
	}
}
