package Levely.Level_2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;

import javax.swing.event.MenuKeyEvent;

public class Pohyb implements KeyListener {
	Vozik vozik;
	public boolean vozikJede;

	public Pohyb(Vozik vozik, Panel panel) {
		this.vozik = vozik;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		pohyb(e);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		pohyb(e);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		vozikJede = false;
	}

	public void pohyb(KeyEvent e) {
		
		switch (e.getKeyChar()) {
		case 'a':
			//System.out.print("<-");
			vozikJede = true;
			vozik.otocenDoPrava = false;
			break;
		case 'd':
			vozikJede = true;
			vozik.otocenDoPrava = true;
			break;
		}
	}
}
